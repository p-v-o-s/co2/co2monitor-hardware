# REV_G

CO2 Monitoring device based on an Adafruit Feather ESP32 and a Sensirion SCD30 CO2 Monitor, and designed as the 'back plate' for a see-through Hammond 1591BTCL case.

![](3d_front.png)

![](3d_back.png)

![](board.png)

![](schematic.svg)
