#!/bin/bash

mv ./kicad/remote.pdf ./schematic.pdf
mv ./kicad/remote.svg ./schematic.svg
git add *
git commit -m $1 
git push
